---
weight: 400
title: Performance
---

# SteamVR Performance tips

{{< hint info >}}

A large amount of info in this guide is sourced from [this guide](https://docs.google.com/document/d/1BdyWxQhFoRkJVfsLvcPNHgvL-esUEE76WbDfguOVbMg/edit) by [Echo](https://twitter.com/EchoTheNeko). Info and pictures were provided by [LiindyVR](https://twitter.com/LiindyVR/status/1734961989015781563), [FlameSoulis](https://twitter.com/FlameSoulis/status/1735106418397602284), [shugy0](https://twitter.com/shugy0), [Echo](https://twitter.com/EchoTheNeko), and [gart](https://twitter.com/gurrrrrrett3).

We've also added Linux specific info to replace the Windows specific info in the original guide.

{{< /hint >}}

## SteamVR

1. In SteamVR settings, turn on Advanced Settings in the bottom left
2. Set your refresh rate to the Highest Possible. This lowers latency even if 
you do not reach those frames.
3. Turn off Motion Smoothing
4. Set Render Resolution to Custom at 100%
5. Turn off Advanced Supersample Filtering
6. Set Overlay Render Quality to Low
7. Click General and set SteamVR Home to Off

{{< hint info >}}

![SteamVR Settings](/images/performance_steamvr_settings.jpg)
Image Credit: [LiindyVR](https://twitter.com/LiindyVR/status/1734961989015781563)

{{< /hint >}}

