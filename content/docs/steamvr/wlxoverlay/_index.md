---
weight: 300
title: WlxOverlay
---

# WlxOverlay

- [WlxOverlay GitHub repository](https://github.com/galister/WlxOverlay)
- [Getting Started Wiki](https://github.com/galister/WlxOverlay/wiki/Getting-Started)
- [Troubleshooting Wiki](https://github.com/galister/WlxOverlay/wiki/Troubleshooting)

WlxOverlay is a tool that lets users interact with their X11 or Wayland desktop from inside VR.

It supports a vast variety of desktop environments, and comes with a fully customizable keyboard.

If you are looking for an overlay for Monado / WiVRn, see [WlxOverlay-X](/docs/fossvr/wlxoverlay-x/).
