---
weight: 100
title: SteamVR
---

# SteamVR

The de facto standard for anything PCVR, SteamVR runs on Linux.

Unfortunately the Linux version is riddled with bugs, missing features and bad performace, so in general it can be a subpar experience.

You can use one of the alternatives listed in the [FOSS VR section](/docs/fossvr/).

If you want to use a standalone headset with SteamVR you can check out [ALVR](/docs/steamvr/alvr/).
