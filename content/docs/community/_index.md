---
weight: 900000
title: Community
---

# Community

You can join us on Matrix or Discord, come hang out!

- [Matrix](https://matrix.to/#/#linux-vr-adventures:matrix.org)
- [Discord](https://discord.gg/e6rRXMPD)
