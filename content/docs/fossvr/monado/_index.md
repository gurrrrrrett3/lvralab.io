---
weight: 100
title: Monado
---

# Monado

- [Monado home page](https://monado.freedesktop.org/)
- [Monado GitLab repository](https://gitlab.freedesktop.org/monado/monado)

> Monado is an open source XR runtime delivering immersive experiences such as VR and AR on mobile, PC/desktop, and other devices. Monado aims to be a complete and conformant implementation of the OpenXR API made by Khronos. The project is currently being developed for GNU/Linux and aims to support other operating systems such as Windows in the near future.

Essentially, Monado is an open source OpenXR implementation, it can be used as an alternative to SteamVR.

Depending on the game, Monado can offer a better overall experience (if with less features) compared to SteamVR, or it might not work at all.

Monado's space may be modified with the enviornment variables OXR_TRACKING_ORIGIN_OFFSET_X=0.0, OXR_TRACKING_ORIGIN_OFFSET_Y=1.0, OXR_TRACKING_ORIGIN_OFFSET_Z=0.0 to force a defined space or origin offset throughout the session. Useful for seated mode configurations, offset units are floating point in meters, positive or negative.

Monado is made for PCVR headsets, if you have a standalone headset you can check out [WiVRn](/docs/fossvr/wivrn/) or [ALVR](/docs/steamvr/alvr/).
