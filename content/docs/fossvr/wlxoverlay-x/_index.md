---
weight: 300
title: WlxOverlay-X
---

# WlxOverlay-X

- [WlxOverlay-X GitHub repository](https://github.com/galister/wlx-overlay-x)

{{< hint danger >}}
**Warning**

WlxOverlay-X is still to be considered alpha-quality and highly experimental.
{{< /hint >}}

WlxOverlay-X is the OpenXR edition of WlxOverlay, the tool that lets users interact with their X11 or Wayland desktop from inside VR.

It supports a vast variety of desktop environments, and comes with a fully customizable keyboard.

WlxOverlay-X works on any runtime that implements EXTX_OVERLAY. Currently this is limited to Monado-based runtimes.

WiVRn users: while WiVRn supports EXTX_OVERLAY, it does no frame composition on the server side, so overlay apps can have ghosting issues.

If you are looking for an overlay for SteamVR, see [WlxOverlay](/docs/steamvr/wlxoverlay/).
