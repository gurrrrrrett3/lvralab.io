---
weight: 70
title: Envision
---

# Envision

- [Envision GitLab repository](https://gitlab.com/gabmus/envision)

Envision is a graphical app that acts as an orchestrator to get a full [Monado](/docs/fossvr/monado/) or [WiVRn](/docs/fossvr/wivrn/) setup up and running with a few clicks.

Envision attempts to construct a working runtime with both a native OpenXR and an OpenVR API, provided by [OpenComposite](/docs/fossvr/opencomposite/), for client aplications to utilize. Please note the OpenVR implementation is incomplete and contains only what's necessary to run most games for compatibility. If you plan to implement software, utilize the OpenXR API, specification [here](https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html).

{{< hint danger >}}
**Warning**

Envision is still to be considered alpha-quality and highly experimental.
{{< /hint >}}

You can download the latest Appimage snapshot from [GitLab Pipelines](https://gitlab.com/gabmus/envision/-/pipelines).
