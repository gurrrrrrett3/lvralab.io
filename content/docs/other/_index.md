---
weight: 300
title: Other
---

# Other

This category houses guides that are not specific to any other cagegory.

- [Dongles over IP](/docs/other/dongles-over-ip/) plug your Watchman dongles into another host on the same network