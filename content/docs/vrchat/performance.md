---
title: Performance
weight: 50
---

# Performance

{{< hint info >}}

A large amount of info in this guide is sourced from [this guide](https://docs.google.com/document/d/1BdyWxQhFoRkJVfsLvcPNHgvL-esUEE76WbDfguOVbMg/edit) by [Echo](https://twitter.com/EchoTheNeko).

{{< /hint >}}

## SteamVR

If you use SteamVR, see [SteamVR Performance tips](/docs/steamvr/performance/).

## FOSS VR

If you use the FOSS VR stack, see [FOSS VR Performance tips](/docs/fossvr/performance/).

## General Optimization

- Make sure your UEFI/BIOS is up to date
- Keep your Chipset drivers updated
- Keep your GPU drivers updated
- Run as few programs as possible
- Close excessive browser tabs
- Clear out bloat from your PC

## VRChat

### Graphics / Graphics Quality

- Graphics Quality: `Custom`
- Anti-Aliasing: `Disabled`
- Mirror Resolution: `Quarter`
- Shadow Quality: `Low`
- LOD Quality: `Low`
- Partical Physics Quality: `Low`

### Graphics / Common

- Particle Limiter: `On`
- Pixel Light Count: `Low`
- Vertical Field of View: `60°`
- Forced Camera Near Distance: `Off`

### Avatars / Avatar Optimizations

- Block Poorly Optimized Avatars: `Very Poor`
- Convert Dynamic Bones to PhysBones: `On`

### Avatars / Avatar Culling

- Hide Avatars Beyond: `20m`
- Maximum Shown Avatars: `15`
- Always Show Friend Avatars: `Off`
- Allow Override with "Show Avatar": `Off`

## Avatar Optimization

Use [d4rkc0d3r's Avatar Optimizer](https://github.com/d4rkc0d3r/d4rkAvatarOptimizer), also follow the VRChat [Avatar Optimization Tips](https://creators.vrchat.com/avatars/avatar-optimizing-tips/).

